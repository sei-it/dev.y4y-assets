$(document).ready(function () {
	
	$(document).on( 'submit', "#form-help" , function( event ) {
	
		event.preventDefault();
		
		// Get some values from elements on the page:
		var $form = $( this ),
		data = $( this ).serialize(),
		url = $form.attr( "action" );
				
		// Send the data using post
		var posting = $.post( url,  data  );
		
		posting.done(function( dt ) {
			if ( dt.errors )
			{
				var err = '';
				
				for(var i=0;i<dt.errors.length;i++)
				{
					var err = err + dt.errors[i];
				}
				
				$('#help-btn').val(err);
				$('#btnAgain').text('Click here to refresh.');
			}
			else
			{
				$('#help-btn').val('Your message has been sent');
				
			}
			$('#help-btn').addClass('aspNetDisabled').prop('disabled',true);
			$('#btnAgain').show();
		});
	});
	
	$('#form-quiz').submit( function (e) {
		e.preventDefault();
		
		validateFormQuiz();
		
		return false;
	});
	
	$('#btn-quiz').click( function (e) {
		e.preventDefault();
		
		validateFormQuiz();
		
		return false;
	});
	
	var validateFormQuiz = function(){
		var $divs = $('#form-quiz div')
		var count = $divs.length;
		var correct_count = 0;
		$divs.each( function(e) {
			var success = false;
			var group_checked = false;
			var $div = $(this);
			var id = $div.attr('data-id');
			$div.find('input:radio').each( function(e2) {
				var $rad = $(this);
				var checked = $rad.attr('checked');
				var val = $rad.val();
				if ( checked )
				{
					group_checked = true;
					if ( val == '1' )
					{
						success = true;
						correct_count++;
					}
					else
					{
						$rad.prop('checked', false);
					}
				}
			});
			if ( group_checked === false )
				$('#require-' + id).show();
			else
				$('#require-' + id).hide();
			if ( success === false )
				$div.removeClass('correct').addClass('incorrect');
			else
				$div.removeClass('incorrect').addClass('correct');
		});
		if ( correct_count < count )
		{
			$('#quiz-results').text( correct_count + ' out of ' + count + ' correct.').show();
			$('#quiz-warning').show();
		}
		else
		{
			$('#quiz-results').text('').hide();
			$('#quiz-warning').hide();
			$('#pnlSuccess').show();
			$divs.removeClass('correct').removeClass('incorrect');
			var page_id = $('#page_id').val();
			$.get('/embeds/track-quiz/' + page_id, function(data) {
				// do nothing
				console.log(data);
			});
		}
	};
	

	var notesChanged = false;
	
	if ($("#notes").length) {
		$("#notes")
			.autosize({
				className: 'autosize',
				append: "\n"
			})
		.keydown(function () {
			$("#savenotes:hidden").slideDown();
			notesChanged = true;
		});

		$("#savenotes").click(function () {
			notesChanged = false;
			$(this).val("Saving...");
		});
		
		function savePrompt()
		{
			if (notesChanged)
			{
				return "You have unsaved notes in your notebook!  If you'd like to save them, please click Save Notes.";
			}
		}
		$(window).bind('beforeunload', savePrompt);
	}
	
    // home carousel
	$('input.gloss-cat').each(function(e){
		var val = '#' + $(this).val();
		$(val).css('opacity', 1);
	});
	
	$(document).on( 'click', ".sign-out" , function( event ) {
		event.preventDefault();
		
		var hrf = $(this).attr('href');
		var formData = {
			ajax 		: 1
		};

		$.ajax({
			url		: "/chat/user/logoutAjax",
			data	: formData,
			type	: "POST",
			success	: function(response){
				$("#ajax_response").html(response);
			}
		});

		$.get(hrf, function(data) {
			location.reload();
		});
	});


	
	$(document).on( 'submit', "#footer-login" , function( event ) {
	
		event.preventDefault();
		
		// Get some values from elements on the page:
		var $form = $( this ),
		data = $( this ).serialize(),
		url = $form.attr( "action" );
				
		// Send the data using post
		var posting = $.post( url,  data  );
		
		posting.done(function( dt ) {
			location.reload();
		});
	});
	
	$(document).on( 'submit', "#forgot_password_form" , function( event ) {
	
		event.preventDefault();
		
		// Get some values from elements on the page:
		var $form = $( this ),
		data = $( this ).serialize(),
		url = $form.attr( "action" );
	
		var posting = $.post( url,  data  );
	
		posting.done(function( dt ) {     
			var title = $(dt).filter( 'title' ).text();
			if (title == 'Error')
			{
				$('#forgot-message-error').slideDown();
			}
			if (title == 'Password Reset Email Sent')
			{
				$('#forgot-message-success').slideDown();
			}
		});
	});
	
	$(document).on( 'submit', "#notes_edit" , function( event ) {
	
		event.preventDefault();
		
		// Get some values from elements on the page:
		var $form = $( this ),
		data = $( this ).serialize(),
		url = $form.attr( "action" );
		
		var cat_id = $('#notes_cat_id').val();
		var mem_id = $('#notes_mem_id').val();
		
		// Send the data using post
		var posting = $.post( url,  data  );
		
		posting.done(function( dt ) {
			if ( dt.success === true)
			{
				$('#savenotes').hide();
				$('#savenotes').val("Save Notes");
				$('#notes-saved').slideDown();
				setTimeout(function () { $('#notes-saved').slideUp(); }, 3000)
			}
			else
			{
				$('#savenotes').val("Error");
			}
			$.get("/embeds/notes-form/" + mem_id + "/" + cat_id, function(ret) {
				$('#notes-form-bucket').html(ret);
				$("#notes")
					.autosize({
						className: 'autosize',
						append: "\n"
					})
					.keydown(function () {
						$("#savenotes:hidden").slideDown();
						notesChanged = true;
					});
			
				$("#savenotes").click(function () {
					notesChanged = false;
					$(this).val("Saving...");
				});
			});
		});
	});

});

$(function () {

	$('.practice-tab').click(function(e) {
		e.preventDefault();
		var targ = $(this).attr('href');
		$('.practice-section').hide();
		$(targ).show();
		$('table.AstkSectionNav tr td').removeClass('Current');
		$(this).parent().addClass('Current');
		return false;
	});
	
	
	var $menu = $('#menu'),
			$menulink = $('#menu-link'),
			$menuTrigger = $('.has-subnav > a');

	$menulink.click(function (e) {
		if ($(document).width() < 900) {
			e.preventDefault();
			$menulink.toggleClass('active');
			$menu.toggleClass('active');
		}
	});

	$menuTrigger.click(function (e) {
		if ($(document).width() < 900) {
			e.preventDefault();
			var $this = $(this);
			$this.toggleClass('active').next('ul').toggleClass('active');
		}
	});


	$("#search-go").click(function () {
		RedirectToSearch("search");
	});

	$("#search-responsive-go").click(function () {
		RedirectToSearch("search-responsive");
	});

	$("#search").keypress(function (e) {
		if (e.which === 13) {
			RedirectToSearch("search");
			return false;
		}
	});

	$("#search-responsive").keypress(function (e) {
		if (e.which === 13) {
			RedirectToSearch("search-responsive");
			return false;
		}
	});

	if (jQuery.ui) {

		$(".selectable").bind('mousedown', function (e) {
			e.metaKey = true;
		}).selectable();
		$(".sortable").sortable();

	}


	function RedirectToSearch(searchBoxId) {
		location.href = "/search/" + encodeURIComponent($("#" + searchBoxId).val().replace("&","").replace("+",""));
	}

	if (!Modernizr.input.placeholder) {

		$('[placeholder]').focus(function () {
			var input = $(this);
			if (input.val() == input.attr('placeholder')) {
				input.val('');
				input.removeClass('placeholder');
			}
		}).blur(function () {
			var input = $(this);
			if (input.val() == '' || input.val() == input.attr('placeholder')) {
				input.addClass('placeholder');
				input.val(input.attr('placeholder'));
			}
		}).blur();
		$('[placeholder]').parents('form').submit(function () {
			$(this).find('[placeholder]').each(function () {
				var input = $(this);
				if (input.val() == input.attr('placeholder')) {
					input.val('');
				}
			})
		});

	}

	if ($(".notify").length) {
		$(".notify").delay(2000).slideUp("slow");
	}

	if ($("[id^=valid]:visible").length)
	{
		$.scrollTo($("[id^=valid]:visible")[0], 500, { offset: -130 });
	}

	if ($(".expanding").length)
	{
		$(".expanding").autosize();
	}


	$(".expandable").children().not("p,.content").click(function () {
			var _parent = $(this).closest(".expandable");
			//Line below would make accordion like effect added by Godfrey 01-15-2016
			//$(".expanded").removeClass("expanded").children("p,.content").slideUp();
			if (_parent.hasClass("expanded"))
			{
				_parent.removeClass("expanded").children("p,.content").slideUp();
			}
			else
			{
			    var firstId = _parent.children(":first").attr("id");
			    if (typeof firstId !== 'undefined' && firstId !== false && firstId.length > 0) {
			        window.location.hash = "#" + _parent.children(":first").attr("id");
			    }
			    _parent.addClass("expanded").find("p,.content").stop().slideDown();
			}
	});

	if (window.location.hash != "")
	{
	    if ($(window.location.hash).length > 0)
	    {
	        $(window.location.hash).click();
	    }
	}


	SetExternalLinks();

});

// set external links to open in new windows and add appropriate classes
function SetExternalLinks() {
	//var hostname = window.location.hostname.replace("www.", "").toLowerCase();
	var hostname = /(y4y\.ed\.gov|staging\.y4y\.seiservices\.com|dev1\.y4y\.seiservices\.com)/;
	$("#content A[href], #footer A[href], .headerlogos A[href]").each(function (i) {
		var href = $(this).attr("href").toLowerCase();
		// if it's not a page on this server
		if ((href.indexOf("http://") != -1 || href.indexOf("https://") != -1) && !href.match(hostname)) {
			// open in a new window
			$(this).attr("target", "_blank");
			// skip things marked skip
			if (!($(this).hasClass("external-skip"))) {
				$(this).addClass("external");
			}
		}
	});



	$("#content .external, #footer .external, .headerlogos .external").click(function() {
		return confirm("You are about to leave y4y.ed.gov and enter an external website. The linked website you wish to visit will be opened in a new window that will appear on top of this one. y4y.ed.gov will still be available in this window.\n\nRemember, our privacy policy is valid only for our site, so be sure to read the privacy policies of other sites, especially if you share any personal information.\n\nThanks for visiting! We hope your visit was useful and enjoyable.\n\nIf you do not wish to visit the link you've clicked, please press Cancel.");
	});


}
// Autosize 1.14 - jQuery plugin for textareas
// (c) 2012 Jack Moore - jacklmoore.com
// license: www.opensource.org/licenses/mit-license.php
(function (e) { var t = { className: "autosizejs", append: "", callback: !1 }, n = "hidden", r = "border-box", i = "lineHeight", s = '<textarea tabindex="-1" style="position:absolute; top:-9999px; left:-9999px; right:auto; bottom:auto; -moz-box-sizing:content-box; -webkit-box-sizing:content-box; box-sizing:content-box; word-wrap:break-word; height:0 !important; min-height:0 !important; overflow:hidden;"/>', o = ["fontFamily", "fontSize", "fontWeight", "fontStyle", "letterSpacing", "textTransform", "wordSpacing", "textIndent"], u = "oninput", a = "onpropertychange", f = e(s)[0]; f.setAttribute(u, "return"), e.isFunction(f[u]) || a in f ? (e(f).css(i, "99px"), e(f).css(i) === "99px" && o.push(i), e.fn.autosize = function (i) { return i = e.extend({}, t, i || {}), this.each(function () { function b() { var e, r, s; p || (p = !0, l.value = t.value + i.append, l.style.overflowY = t.style.overflowY, s = parseInt(t.style.height, 10), l.style.width = f.css("width"), l.scrollTop = 0, l.scrollTop = 9e4, e = l.scrollTop, r = n, e > h ? (e = h, r = "scroll") : e < c && (e = c), e += m, t.style.overflowY = r, s !== e && (t.style.height = e + "px", y && i.callback.call(t)), setTimeout(function () { p = !1 }, 1)) } var t = this, f = e(t), l, c = f.height(), h = parseInt(f.css("maxHeight"), 10), p, d = o.length, v, m = 0, g = t.value, y = e.isFunction(i.callback); if (f.css("box-sizing") === r || f.css("-moz-box-sizing") === r || f.css("-webkit-box-sizing") === r) m = f.outerHeight() - f.height(); if (f.data("mirror") || f.data("ismirror")) return; l = e(s).data("ismirror", !0).addClass(i.className)[0], v = f.css("resize") === "none" ? "none" : "horizontal", f.data("mirror", e(l)).css({ overflow: n, overflowY: n, wordWrap: "break-word", resize: v }), h = h && h > 0 ? h : 9e4; while (d--) l.style[o[d]] = f.css(o[d]); e("body").append(l), a in t ? u in t ? t[u] = t.onkeyup = b : t[a] = b : (t[u] = b, t.value = "", t.value = g), e(window).resize(b), f.bind("autosize", b), b() }) }) : e.fn.autosize = function (e) { return this } })(jQuery);
/**
 * Copyright (c) 2007-2012 Ariel Flesler - aflesler(at)gmail(dot)com | http://flesler.blogspot.com
 * Dual licensed under MIT and GPL.
 * @author Ariel Flesler
 * @version 1.4.3.1
 */
; (function ($) { var h = $.scrollTo = function (a, b, c) { $(window).scrollTo(a, b, c) }; h.defaults = { axis: 'xy', duration: parseFloat($.fn.jquery) >= 1.3 ? 0 : 1, limit: true }; h.window = function (a) { return $(window)._scrollable() }; $.fn._scrollable = function () { return this.map(function () { var a = this, isWin = !a.nodeName || $.inArray(a.nodeName.toLowerCase(), ['iframe', '#document', 'html', 'body']) != -1; if (!isWin) return a; var b = (a.contentWindow || a).document || a.ownerDocument || a; return /webkit/i.test(navigator.userAgent) || b.compatMode == 'BackCompat' ? b.body : b.documentElement }) }; $.fn.scrollTo = function (e, f, g) { if (typeof f == 'object') { g = f; f = 0 } if (typeof g == 'function') g = { onAfter: g }; if (e == 'max') e = 9e9; g = $.extend({}, h.defaults, g); f = f || g.duration; g.queue = g.queue && g.axis.length > 1; if (g.queue) f /= 2; g.offset = both(g.offset); g.over = both(g.over); return this._scrollable().each(function () { if (e == null) return; var d = this, $elem = $(d), targ = e, toff, attr = {}, win = $elem.is('html,body'); switch (typeof targ) { case 'number': case 'string': if (/^([+-]=)?\d+(\.\d+)?(px|%)?$/.test(targ)) { targ = both(targ); break } targ = $(targ, this); if (!targ.length) return; case 'object': if (targ.is || targ.style) toff = (targ = $(targ)).offset() } $.each(g.axis.split(''), function (i, a) { var b = a == 'x' ? 'Left' : 'Top', pos = b.toLowerCase(), key = 'scroll' + b, old = d[key], max = h.max(d, a); if (toff) { attr[key] = toff[pos] + (win ? 0 : old - $elem.offset()[pos]); if (g.margin) { attr[key] -= parseInt(targ.css('margin' + b)) || 0; attr[key] -= parseInt(targ.css('border' + b + 'Width')) || 0 } attr[key] += g.offset[pos] || 0; if (g.over[pos]) attr[key] += targ[a == 'x' ? 'width' : 'height']() * g.over[pos] } else { var c = targ[pos]; attr[key] = c.slice && c.slice(-1) == '%' ? parseFloat(c) / 100 * max : c } if (g.limit && /^\d+$/.test(attr[key])) attr[key] = attr[key] <= 0 ? 0 : Math.min(attr[key], max); if (!i && g.queue) { if (old != attr[key]) animate(g.onAfterFirst); delete attr[key] } }); animate(g.onAfter); function animate(a) { $elem.animate(attr, f, g.easing, a && function () { a.call(this, e, g) }) } }).end() }; h.max = function (a, b) { var c = b == 'x' ? 'Width' : 'Height', scroll = 'scroll' + c; if (!$(a).is('html,body')) return a[scroll] - $(a)[c.toLowerCase()](); var d = 'client' + c, html = a.ownerDocument.documentElement, body = a.ownerDocument.body; return Math.max(html[scroll], body[scroll]) - Math.min(html[d], body[d]) }; function both(a) { return typeof a == 'object' ? a : { top: a, left: a } } })(jQuery);
/*
 * Expandable, a jQuery plugin to dynamically group and hide web content
 * Copyright (C) 2009  Marc Diethelm
 * License: (GPL 3, http://www.gnu.org/licenses/gpl-3.0.txt) see license.txt
 */

(function (a) { a.fn.expandable = function () { var c; var d = {}; if (arguments.length == 1) { c = (arguments[0].constructor == String ? arguments[0] : null); d = (arguments[0].constructor == Object ? arguments[0] : null) } else { if (arguments.length == 2) { c = (arguments[0].constructor == String ? arguments[0] : null); d = arguments[1] } } if (c == "state") { return a(this).eq(0).data("state") } var b = a.extend({}, a.fn.expandable.defaults, d); return this.each(function () { var m = a(this); switch (c) { case "destroy": m.removeClass("ui-widget ui-expandable ui-expandable-open"); a(".ui-widget-content", this).remove().contents().appendTo(this); if (m.data("elTitle")) { a(".ui-widget-header", this).unbind("click").remove(); m.prepend(m.data("elTitle")) } return this; case "close": this.closeExpandable(null, b); return this; case "open": this.openExpandable(null, b); return this } m.hide().addClass("ui-expandable ui-widget"); var p = ""; if (a(".ui-expandable-title", this).length > 0) { var n = a(".ui-expandable-title", this).eq(0).remove(); p = n.text(); m.data("elTitle", n); delete n } p = b.title || p; if (a(".ui-widget-content", this).length == 0) { var j = '<div class="ui-widget-content ui-helper-clearfix"></div>'; if (m.contents().length) { m.contents().wrapAll(j) } else { m.html(j) } } var h = a(".ui-widget-content", this); if (b.startopen) { m.data("state", "open") } else { m.data("state", "closed"); h.hide() } if (b.uiIconClosed && b.uiIconOpen) { var o = (b.startopen ? b.uiIconOpen : b.uiIconClosed); var g = b.uiIconClosed; var f = b.uiIconOpen } else { var o = (b.startopen ? "icon-open" : "icon-closed"); var g = "icon-closed"; var f = "icon-open" } var l = ""; if (b.extraicon) { l = "ui-icon " + b.extraicon } var e = a('<div class="ui-state-default ui-widget-header" title="' + b.tooltip + '">	<div class="ui-expandable-icon ui-icon ' + o + '"></div>	<div class="ui-expandable-title">' + p + '</div>	<div class="ui-expandable-extraicon"><span class="' + l + '"></span></div></div>'); a(".ui-widget-header", this).length ? a(".ui-widget-header", this).replaceWith(e) : m.prepend(e); m.show(); a(".ui-icon", e).each(function () { var q, r = 0; if (e.innerHeight() > 0) { var s = e.innerHeight() - a(this).height(); q = Math.floor((s / 2)); var r = s - q } else { if (b.iconMarginTop) { q = b.iconMarginTop; r = b.iconMarginBottom || 0 } } if (q > 0) { a(this).css({ "margin-top": q, "margin-bottom": r }) } }); var k = this; e.bind("click", null, function (q) { var r = m.data("state"); if (b.headerClick) { if (b.headerClick.call(k, q, b) === false) { return false } } if (r == "closed" || r == "closing") { k.openExpandable.call(k, q, b) } else { k.closeExpandable.call(k, q, b) } return true }); this.closeExpandable = function (q, r) { a(".ui-expandable-icon", e).removeClass(f).addClass(g); m.data("state", "closing"); if (r.closing) { r.closing.call(this, q, r) } -h.animate(r.animationClose, r.duration, r.easing, function () { m.data("state", "closed"); if (r.closed) { r.closed.call(k, q, r) } }) }; this.openExpandable = function (q, r) { a(".ui-expandable-icon", e).removeClass(g).addClass(f); m.data("state", "opening"); if (r.opening) { r.opening.call(this, q, r) } -h.animate(r.animationOpen, r.duration, r.easing, function () { m.data("state", "open"); if (r.open) { r.open.call(k, q, r) } }) }; if (b.headerHover) { e.hover(function (q) { return b.headerHover.call(e, true, q, b) }, function (q) { return b.headerHover.call(e, false, q, b) }) } if (b.extraiconClick) { a(".ui-expandable-extraicon", this).click(function (q) { return b.extraiconClick.call(k, q, b) }) } if (b.extraiconHover) { var i = a(".ui-expandable-extraicon", k); i.hover(function (q) { return b.extraiconHover.call(i.get(0), true, q, b) }, function (q) { return b.extraiconHover.call(i.get(0), false, q, b) }) } return this }) }; a.fn.expandable.defaults = { startopen: false, title: null, tooltip: "Click to expand", uiIconClosed: "ui-icon-triangle-1-e", uiIconOpen: "ui-icon-triangle-1-s", animationClose: { height: "hide" }, animationOpen: { height: "show" }, duration: "normal", easing: "swing", open: null, closed: null, opening: null, closing: null, headerClick: null, headerHover: function (c, b) { if (c) { a(this).removeClass("ui-state-default").addClass("ui-state-hover") } else { a(this).removeClass("ui-state-hover").addClass("ui-state-default") } }, extraicon: null, extraiconClick: null, extraiconHover: null, extraiconHover: function (d, c, b) { if (d) { a(this).addClass("ui-state-hover") } else { a(this).removeClass("ui-state-hover") } }, iconMarginTop: null, iconMarginBottom: null } })(jQuery);

if (typeof Object.create !== "function") {
	Object.create = function (o) {
		function F() {
		};
		F.prototype = o;
		return new F();
	};
}
var ua = {
	toString: function () {
		return navigator.userAgent;
	}, test: function (s) {
		return this.toString().toLowerCase().indexOf(s.toLowerCase()) > -1;
	}
};
ua.version = (ua.toString().toLowerCase().match(/[\s\S]+(?:rv|it|ra|ie)[\/: ]([\d.]+)/) || [])[1];
ua.webkit = ua.test("webkit");
ua.gecko = ua.test("gecko") && !ua.webkit;
ua.opera = ua.test("opera");
ua.ie = ua.test("msie") && !ua.opera;
ua.ie6 = ua.ie && document.compatMode && typeof document.documentElement.style.maxHeight === "undefined";
ua.ie7 = ua.ie && document.documentElement && typeof document.documentElement.style.maxHeight !== "undefined" && typeof XDomainRequest === "undefined";
ua.ie8 = ua.ie && typeof XDomainRequest !== "undefined";
var domReady = function () {
	var _1 = [];
	var _2 = function () {
		if (!arguments.callee.done) {
			arguments.callee.done = true;
			for (var i = 0; i < _1.length; i++) {
				_1[i]();
			}
		}
	};
	if (document.addEventListener) {
		document.addEventListener("DOMContentLoaded", _2, false);
	}
	if (ua.ie) {
		(function () {
			try {
				document.documentElement.doScroll("left");
			}
			catch (e) {
				setTimeout(arguments.callee, 50);
				return;
			}
			_2();
		})();
		document.onreadystatechange = function () {
			if (document.readyState === "complete") {
				document.onreadystatechange = null;
				_2();
			}
		};
	}
	if (ua.webkit && document.readyState) {
		(function () {
			if (document.readyState !== "loading") {
				_2();
			} else {
				setTimeout(arguments.callee, 10);
			}
		})();
	}
	window.onload = _2;
	return function (fn) {
		if (typeof fn === "function") {
			_1[_1.length] = fn;
		}
		return fn;
	};
}();

/*
* Print Element Plugin 1.2
* Copyright (c) 2010 Erik Zaadi
*  Home Page : http://projects.erikzaadi/jQueryPlugins/jQuery.printElement 
*  jQuery plugin page : http://plugins.jquery.com/project/printElement 
* Dual licensed under the MIT and GPL licenses:
*   http://www.opensource.org/licenses/mit-license.php
*   http://www.gnu.org/licenses/gpl.html
*   Note, Iframe Printing is not supported in Opera and Chrome 3.0, a popup window will be shown instead
*/
; (function (g) {
	function k(c) { c && c.printPage ? c.printPage() : setTimeout(function () { k(c) }, 50) } function l(c) {
		c = a(c); a(":checked", c).each(function () { this.setAttribute("checked", "checked") }); a("input[type='text']", c).each(function () { this.setAttribute("value", a(this).val()) }); a("select", c).each(function () { var b = a(this); a("option", b).each(function () { b.val() == a(this).val() && this.setAttribute("selected", "selected") }) }); a("textarea", c).each(function () {
			var b = a(this).attr("value"); if (a.browser.b && this.firstChild) this.firstChild.textContent =
			b; else this.innerHTML = b
		}); return a("<div></div>").append(c.clone()).html()
	} function m(c, b) {
		var i = a(c); c = l(c); var d = []; d.push("<html><head><title>" + b.pageTitle + "</title>"); if (b.overrideElementCSS) { if (b.overrideElementCSS.length > 0) for (var f = 0; f < b.overrideElementCSS.length; f++) { var e = b.overrideElementCSS[f]; typeof e == "string" ? d.push('<link type="text/css" rel="stylesheet" href="' + e + '" >') : d.push('<link type="text/css" rel="stylesheet" href="' + e.href + '" media="' + e.media + '" >') } } else a("link", j).filter(function () {
			return a(this).attr("rel").toLowerCase() ==
			"stylesheet"
		}).each(function () { d.push('<link type="text/css" rel="stylesheet" href="' + a(this).attr("href") + '" media="' + a(this).attr("media") + '" >') }); d.push('<base href="' + (g.location.protocol + "//" + g.location.hostname + (g.location.port ? ":" + g.location.port : "") + g.location.pathname) + '" />'); d.push('</head><body style="' + b.printBodyOptions.styleToAdd + '" class="' + b.printBodyOptions.classNameToAdd + '">'); d.push('<div class="' + i.attr("class") + '">' + c + "</div>"); d.push('<script type="text/javascript">function printPage(){focus();print();' +
		(!a.browser.opera && !b.leaveOpen && b.printMode.toLowerCase() == "popup" ? "close();" : "") + "}<\/script>"); d.push("</body></html>"); return d.join("")
	} var j = g.document, a = g.jQuery; a.fn.printElement = function (c) {
		var b = a.extend({}, a.fn.printElement.defaults, c); if (b.printMode == "iframe") if (a.browser.opera || /chrome/.test(navigator.userAgent.toLowerCase())) b.printMode = "popup"; a("[id^='printElement_']").remove(); return this.each(function () {
			var i = a.a ? a.extend({}, b, a(this).data()) : b, d = a(this); d = m(d, i); var f = null, e = null;
			if (i.printMode.toLowerCase() == "popup") { f = g.open("about:blank", "printElementWindow", "width=650,height=440,scrollbars=yes"); e = f.document } else {
				f = "printElement_" + Math.round(Math.random() * 99999).toString(); var h = j.createElement("IFRAME"); a(h).attr({ style: i.iframeElementOptions.styleToAdd, id: f, className: i.iframeElementOptions.classNameToAdd, frameBorder: 0, scrolling: "no", src: "about:blank" }); j.body.appendChild(h); e = h.contentWindow || h.contentDocument; if (e.document) e = e.document; h = j.frames ? j.frames[f] : j.getElementById(f);
				f = h.contentWindow || h
			} focus(); e.open(); e.write(d); e.close(); k(f)
		})
	}; a.fn.printElement.defaults = { printMode: "iframe", pageTitle: "", overrideElementCSS: null, printBodyOptions: { styleToAdd: "padding:10px;margin:10px;", classNameToAdd: "" }, leaveOpen: false, iframeElementOptions: { styleToAdd: "border:none;position:absolute;width:0px;height:0px;bottom:0px;left:0px;", classNameToAdd: "" } }; a.fn.printElement.cssElement = { href: "", media: "" }
})(window);
