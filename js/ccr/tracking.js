/**
 * Created by godfreymajwega on 12/22/15.
 */

function logVisit(slide, member){

    var form_data = {
        action      :       'logVisit',
        slide_id    :       slide,
        member_id   :       member
    };
    $.ajax({
        url: "/members/ccr_tracking/logVisit",
        type: 'POST',
        data: form_data,
        success: function (response) {
            alert(response);
        }
    });
}

function populateGlobalVars(entry){

    var form_data = {
        action      :       'populateGlobalVars',
        entry_id    :       entry
    };
    $.ajax({
        url: "/members/ccr_tracking/populateGlobalVars",
        type: 'POST',
        data: form_data,
        success: function (response) {
            alert(response);
        }
    });
}

